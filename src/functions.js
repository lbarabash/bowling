function createRandomNum(max) {
    return Math.floor(Math.random() * (max + 1));
}

function getInitialTable() {
    return {
        1: {
            1: null,
            2: null
        },
        2: {
            1: null,
            2: null
        },
        3: {
            1: null,
            2: null
        },
        4: {
            1: null,
            2: null
        },
        5: {
            1: null,
            2: null
        },
        6: {
            1: null,
            2: null
        },
        7: {
            1: null,
            2: null
        },
        8: {
            1: null,
            2: null
        },
        9: {
            1: null,
            2: null
        },
        10: {
            1: null,
            2: null,
            3: null
        }
    };
}

function rollBetween(table, min, max, frameNumber, rollNumber) {
    return table[frameNumber][rollNumber] >= min && table[frameNumber][rollNumber] <= max;
}

if (typeof(module) !== 'undefined') {
    module.exports = {
        createRandomNum: createRandomNum,
        getInitialTable: getInitialTable,
        rollBetween: rollBetween
    };
}
