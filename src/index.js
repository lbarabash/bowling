const buttonRoll = document.querySelector('.roll');
const buttonReset = document.querySelector('.reset');
let boxOne = document.querySelectorAll('.box-one');
let boxTwo = document.querySelectorAll('.box-two');
let boxThree = document.querySelector('.box-three');
let boxScore = document.querySelectorAll('.score-box');
const textGameOver = document.querySelector('.gameover');

let frameCounter = 1;
let clickCounter = 1;
let randomNumber;

let table = getInitialTable();

function calculateResult() {
    let randomNumber = createRandomNum(10);
    if (frameCounter < 10) {
        if (clickCounter === 1) {
            table[frameCounter][clickCounter] = randomNumber;
            clickCounter += 1;
        } else {
            table[frameCounter][clickCounter] = createRandomNum(Math.abs(table[frameCounter][1] - 10));
            frameCounter += 1;
            clickCounter = 1;
        }
    } else if (frameCounter === 10) {
        if (clickCounter === 1) {
            table[frameCounter][clickCounter] = randomNumber;
            clickCounter += 1;
        } else if (clickCounter === 2) {
            table[frameCounter][clickCounter] = createRandomNum(Math.abs(table[frameCounter][1] - 10));
            clickCounter += 1;
        } else {
            table[frameCounter][clickCounter] = createRandomNum(10);
            frameCounter += 1;
        }
    }
    showResult();
};

function firstRollBetween(min, max, frameNumber) {
    return rollBetween(table, min, max, frameNumber, 1);
}

function secondRollBetween(min, max, frameNumber) {
    return rollBetween(table, min, max, frameNumber, 2);
}

function rollEqual(frameNumber, rollNumber, value) {
    return table[frameNumber][rollNumber] === value;
}

function firstRollEqual(frameNumber, value) {
    return rollEqual(frameNumber, 1, value);
}

function secondRollEqual(frameNumber, value) {
    return rollEqual(frameNumber, 2, value);
}

function calculateBoxScore(frameNumber) {
    if (frameNumber < 10) {
        if (firstRollEqual(frameNumber, 10) && firstRollEqual(frameNumber + 1, 10)) {
            if (table[frameNumber + 2] === undefined) {
                boxScore[frameNumber - 1].innerHTML = table[frameNumber][1] + table[frameNumber + 1][1];
            } else if (table[frameNumber + 1] === undefined) {
                boxScore[frameNumber - 1].innerHTML = table[frameNumber][1];
            } else {
                boxScore[frameNumber - 1].innerHTML = table[frameNumber][1] + table[frameNumber + 1][1]
                    + table[frameNumber + 2][1];
            }
        } else if (firstRollEqual(frameNumber, 10) && table[frameNumber + 1][1] < 10) {
            boxScore[frameNumber - 1].innerHTML = table[frameNumber][1] + table[frameNumber + 1][1]
                + table[frameNumber + 1][2];
        } else if (table[frameNumber][1] + table[frameNumber][2] === 10) {
            boxScore[frameNumber - 1].innerHTML = table[frameNumber][1] + table[frameNumber][2]
                + table[frameNumber + 1][1];
        } else {
            boxScore[frameNumber - 1].innerHTML = table[frameNumber][1] + table[frameNumber][2];
        }
    } else if (frameNumber === 10) {
        boxThree.innerHTML = table[frameNumber][3];
        boxScore[frameNumber - 1].innerHTML = table[frameNumber][1] + table[frameNumber][2] + table[frameNumber][3];
    }
    if (boxScore[frameNumber - 2] !== undefined) {
        boxScore[frameNumber - 1].innerHTML = Number(boxScore[frameNumber - 1].innerHTML)
            + Number(boxScore[frameNumber - 2].innerHTML);
    }
}

function showResult() {
    for (let i = 1; i <= frameCounter; i++) {
        if (i > 10) {
            buttonRoll.removeEventListener('click', calculateResult);
            textGameOver.classList.add('active');
            buttonRoll.classList.add('disabled');
            break;
        }
        if (firstRollEqual(i, 0) && secondRollEqual(i, 0)) {
            boxOne[i - 1].innerHTML = '-';
            boxTwo[i - 1].innerHTML = '-';
            boxThree.innerHTML = '';
        }
        if (firstRollEqual(i, 10)) {
            boxOne[i - 1].innerHTML = '';
            boxTwo[i - 1].innerHTML = 'X';
        }
        if (firstRollBetween(1, 9, i) && secondRollEqual(i, 0)) {
            boxOne[i - 1].innerHTML = table[i][1];
            boxTwo[i - 1].innerHTML = '-';
        }
        if (firstRollBetween(1, 9, i) && secondRollBetween(1, 9, i)) {
            boxOne[i - 1].innerHTML = table[i][1];
            boxTwo[i - 1].innerHTML = table[i][2];
        }
        if (firstRollEqual(i, 0) && (secondRollEqual(i, 10) || secondRollBetween(1, 9, i))) {
            boxOne[i - 1].innerHTML = '-';
            boxTwo[i - 1].innerHTML = table[i][2];
        }
        if (firstRollEqual(i, 0) && secondRollEqual(i, null)) {
            boxOne[i - 1].innerHTML = '-';
            boxTwo[i - 1].innerHTML = '';
        }
        if (firstRollBetween(1, 9, i) && secondRollEqual(i, null)) {
            boxOne[i - 1].innerHTML = table[i][1];
            boxTwo[i - 1].innerHTML = '';
        }
        if (table[i][1] + table[i][2] === 10 && table[i][1] !== 10) {
            boxOne[i - 1].innerHTML = table[i][1];
            boxTwo[i - 1].innerHTML = '/';
        }
        calculateBoxScore(i);
    }
    for (let i = frameCounter + 1; i <= 10; i++) {
        boxOne[i - 1].innerHTML = '';
        boxTwo[i - 1].innerHTML = '';
        boxScore[i - 1].innerHTML = '';
        boxThree.innerHTML = '';
    }
}

function drawInitialTable() {
    boxThree.innerHTML = '';
    for (let i = 0; i < 10; i++) {
        boxOne[i].innerHTML = '';
        boxTwo[i].innerHTML = '';
        boxScore[i].innerHTML = '';
    }
}

function resetGame() {
    frameCounter = 1;
    clickCounter = 1;
    buttonRoll.addEventListener('click', calculateResult);
    textGameOver.classList.remove('active');
    buttonRoll.classList.remove('disabled');
    table = getInitialTable();
    drawInitialTable();
}
buttonRoll.addEventListener('click', calculateResult);
buttonReset.addEventListener('click', resetGame);
