const expect = require('chai').expect;
const createRandomNum = require('../src/functions').createRandomNum;
const getInitialTable = require('../src/functions').getInitialTable;
const rollBetween = require('../src/functions').rollBetween;

describe('createRandomNum', function () {
    it('createRandomNum(10) returns the number between 0 and 10', function () {
        expect(
            createRandomNum(10)
        ).to.be.at.least(0).and.to.be.at.most(10);
    });
    it('createRandomNum(3) returns the number between 0 and 3', function () {
        expect(
            createRandomNum(3)
        ).to.be.at.least(0).and.to.be.at.most(3);
    });
});

describe('getInitialTable', function () {
    it('getInitialTable returns object', function () {
        expect(
            typeof(getInitialTable())
        ).to.be.equal('object');
    });
    it('getInitialTable returns object with 10 keys', function () {
        expect(
            Object.keys(getInitialTable()).length
        ).to.be.equal(10);
    });
});

describe('rollBetween', function () {
    it('rollBetween returns false', function () {
        let table = getInitialTable();
        expect(
            rollBetween(table, 1, 9, 1, 1)
        ).to.be.equal(false);
    });
    it('rollBetween returns true', function () {
        let table = getInitialTable();
        table[1][1] = 6;
        expect(
            rollBetween(table, 1, 9, 1, 1)
        ).to.be.equal(true);
    });
    it('rollBetween returns true', function () {
        let table = getInitialTable();
        table[1][2] = 4;
        expect(
            rollBetween(table, 1, 9, 1, 2)
        ).to.be.equal(true);
    });
});
